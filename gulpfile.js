const elixir = require('laravel-elixir');


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.assetsPath = 'workbench/introcept/core/resources/assets';
elixir.config.publicPath = 'public/js/core';

elixir(function(mix) {
	mix.scripts(["script.js"], elixir.config.publicPath + "/scripts.js")
    //    		.webpack('app.js');
});
