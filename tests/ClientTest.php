<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ClientTest extends TestCase
{

    private $newClientInfo = [
        'name' => 'unit test user',
        'gender' => 'male',
        'phone' => '9841212121',
        'email' => 'unittest1@gmail.com',
        'address' => ' Nepal',
        'nationality' => 'Nepal',
        'dob' => '1998-04-26',
        'education' => 'Bachelors',
        'contact_way' => 'phone',
    ];

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testClientHomePage()
    {
        echo ".....Initiating Test........\n";
        $this->visit("/clients");
        echo ".....Client Home Page Running Well.....\n";

    }

    public function testAddClient()
    {
        echo "....Adding a new Client... \n";
        $this->visit("clients")
            ->click("Add Clients")
            ->seePageIs('/clients/create')
            ->submitForm('Submit', $this->newClientInfo)
            ->seePageIs('/clients');
        echo "...Addding New User Completed... \n";
    }

}     