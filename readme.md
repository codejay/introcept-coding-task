## Introcept Coding Task

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

This Project includes a simple create and read part of a Client Management System.It is done in Laravel 5.3 a popular PHP Framework.As per suggested , the task was divided and a rough sketch was made on how to carry on and accomplish the task and it was done accordingly.I took help from 

http://kamranahmed.info/blog/2015/12/03/creating-a-modular-application-in-laravel/

http://laraveldaily.com/how-to-create-a-laravel-5-package-in-10-easy-steps/

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Brief Overview

I used modular approach so as to make the project more reusable,more debuggable and work friendly in the future.So the Codes can be found inside of the workbench/introcept/core.

## Quick Demonstration

[Heroku](http://fathomless-headland-17498.herokuapp.com/clients)

## How can i run this

Project can be cloned from [Coding Task](git@bitbucket.org:jay169/introcept-coding-task.git).
After cloning hit
 
composer install

npm install gulp 

npm install 

laravel-elixir

gulp

Please consider using sudo where necessary
run gulp 
please serve the applicatiion with php artisan serve, hopefully it should work.
The .env file is already included and down in the bottom there should be a field called "LOGENTRIES_TOKEN".
You need to paste your log entries token over there.
It already has a token though , own token can also be created.
The following brief guide may help you create log entries token [create Log Entry Token](https://gist.github.com/coderjay12/04b7f76a94a0fa567776d33abf150d74).
Then the application should run along with  keeping the track about the activities going with the system.
It should look like below:


![Screenshot from 2016-10-16 18-16-15.png](https://bitbucket.org/repo/8rKrME/images/229291358-Screenshot%20from%202016-10-16%2018-16-15.png)

## Technical Specification 
* Design Approach : Modular Approach
* Design Pattern : Repository Pattern
* Autoload and Indentation : PSR-4 and PSR-2
* For Log purpose : LogEntries.
* Deployment : heroku

## Code Quality Check

An Online code cheker [Codacy](https://www.codacy.com/) was used to check the quality of codes..
The results of this project can be viewed [Code Check](https://www.codacy.com/app/duwaljyoti16/Introcept-Coding-Task/dashboard)

## Test 
For Testing purpose one can go into terminal and type php unit.The test will check if the page is correctly visited and also will add one user.

## Little Documentation on How i deployed it into Heorku
[Heroku Deployment](https://gist.github.com/coderjay12/c03a96987c9fd9495d28a62da208bc11)

## Libraries and External Packages used
* phpunit
* jquery datatables
* bootstrap as a front end framework 


## References
* Stackoverflow (encounter problems during development)
* Related Documentation for Heroku and Log Entries