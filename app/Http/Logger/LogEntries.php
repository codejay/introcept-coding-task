<?php
namespace App\Http\Logger ;

class LogEntries
{
	
/**********
*  BEGIN - User - Defined Variables
***********/


	// put your Logentries Log Token inside the double quotes in the $LOGENTRIES_TOKEN constant below.
  	
  // public $LOGENTRIES_TOKEN = "f9086047-474b-435c-977e-ddad73e4cbd2";

/*  
*	To Send Log Events To Your DataHub, Change The Following Variables
*		1. Change the $DATAHUB_ENABLED variable to true;	
*		2. IP Address of your datahub location  
*		3. Set the Port for communicating with Datahub (10000 default) 
*
*		NOTE: If $DATAHUB_ENABLED = true, Datahub will ignore your Logentries log token as it is not required when using Datahub.
*/
	
	public $DATAHUB_ENABLED = false;
	
	
	// Your DataHub IP Address MUST be specified if $DATAHUB_ENABLED = true
 	
 	public $DATAHUB_IP_ADDRESS = "";
	
		
	//	  Default port for DataHub is 10000, 
	//    If you change this from port 10000, you will have to change your settings port on your datahub machine, 
	//	  specifically in the datahub local config file in /etc/leproxy/leproxyLocal.config then restart leproxy - sudo service leproxy restart
	
	public $DATAHUB_PORT = 10000;	
	
	
	// Allow Your Host Name To Be Printed To Your Log Events As Key / Value Pairs.
	// To give your Log events a Host_Name which will appear in your logs as Key Value Pairs, change this value to 'true' (without quotes)

	public $HOST_NAME_ENABLED = false;

	
	// Enter a Customized Host Name to appear in your Logs - If no host name is entered one will be assigned based on your own Host name for the local machine using the php function gethostname();

	public $HOST_NAME = "";
 
	
	
	// Enter a Host ID to appear in your Log events
	// if $HOST_ID is empty "", it wil not print to your log events.  This value will only print to your log events if there is a value below as in $HOST_ID="12345".
	
	public $HOST_ID = "";

	
	// Add the local server timestamp to each log item

public	$ADD_LOCAL_TIMESTAMP = true;
	
	
	
/************
*  END  -  User - Defined Variables
************/

	
	

	// Whether the socket is persistent or not
	public $Persistent = true;

	// Whether the socket uses SSL/TLS or not
	 public $SSL = false;
	
	// Set the minimum severity of events to send
	public $Severity = LOG_DEBUG;
	/*
	 *  END  User - Defined Variables
	 */

	// // Ignore this, used for PaaS that support configuration variables
	//  public $ENV_TOKEN = getenv('LOGENTRIES_TOKEN');
	
	// // Check for environment variable first and override LOGENTRIES_TOKEN variable accordingly
	// if ($ENV_TOKEN != false && $LOGENTRIES_TOKEN === "")
	// {
	// 	$LOGENTRIES_TOKEN = $ENV_TOKEN;
	// }
	
	public function getLogEntries()
	{
		return env("LOGENTRIES_TOKEN", "");
	}

	public function getLog()
	{
		$log = LeLogger::getLogger($this->getLogEntries() ? : "f9086047-474b-435c-977e-ddad73e4cbd2", $this->Persistent, $this->SSL, $this->Severity, $this->DATAHUB_ENABLED, $this->DATAHUB_IP_ADDRESS, $this->DATAHUB_PORT, $this->HOST_ID, $this->HOST_NAME, $this->HOST_NAME_ENABLED, $this->ADD_LOCAL_TIMESTAMP);

		return $log ; 
	}

	
}