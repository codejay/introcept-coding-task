<?php


Route::group(['middleware' => 'web'], function () {
    Route::resource("/clients", "Introcept\Core\Http\Controllers\ClientController");

    Route::get("/validate-email", "Introcept\Core\Http\Controllers\ClientController@validateUsersEmail");
    Route::get("/validate-name", "Introcept\Core\Http\Controllers\ClientController@validateUsersName");
});



