<?php

namespace Introcept\Core\Http\Controllers;

use Validator;
use \App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Introcept\Core\Http\Repositories\ClientRepository;
use Introcept\Core\Http\Forms\ClientForm;
use App\Http\Logger\LogEntries;

class ClientController extends Controller
{
    protected $client;
    protected $ClientForm;
    protected $logger;

    /**
     * @param ClientForm $ClientForm
     * @param ClientForm $ClientForm
     * @return mixed
     */
    public function __construct(ClientForm $ClientForm, ClientRepository $client, LogEntries $logEntries)
    {
        $this->middleware("web");
        $this->ClientForm = $ClientForm;
        $this->client = $client;
        $this->logEntries = $logEntries->getLog();
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $clients = $this->client->all();
        $this->logEntries->Info("Client Index page viewed");

        return view("core::clients.index", compact("clients"));
    }

    public function create()
    {
        $countries = $this->getCountryName();
        return view("core::clients.add-client", compact("countries"));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $clientInfo = $request->except("_token");
        $newClientValidator = Validator::make($clientInfo, $this->ClientForm->getRules());
        if ($newClientValidator->fails()) {

            return \Redirect::back()->withErrors($newClientValidator);
        }

        $this->client->storeData($clientInfo);
        $this->logEntries->Notice("New User was Added to the System");

        return redirect()->route('clients.index')->withMessage('Successfully added a new Client.');
    }

    /**
     * @param $clientId
     * @return mixed
     */
    public function show($clientId)
    {
        $clientInfo = $this->client->getInfo($clientId);
        $this->logEntries->Info("User Profile Viewed");

        return view('core::clients.detail', compact("clientInfo"));
    }

    /**
     * @param $clientId
     * @return mixed
     */
    public function edit($clientId)
    {

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request)
    {

    }

    /**
     * @param $clientId
     * @return mixed
     */
    public function destroy($clientId)
    {

    }

    public function validateUsersEmail()
    {
        foreach ($this->client->all() as $client) {
            if ($_GET['email'] == $client["Email"])
                return "exists";
        }

        return "unique";
    }

    public function validateUsersName()
    {
        foreach ($this->client->all() as $client) {
            if (str_replace("%20", " ", $_GET['name']) == $client["Name"])
                return "exists";
        }

        return "unique";
    }

    public function getCountryName()
    {
        return [
            "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"];
    }


}
