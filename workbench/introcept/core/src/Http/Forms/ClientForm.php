<?php

namespace Introcept\Core\Http\Forms ; 

class ClientForm
{

    public function getRules()
    {
        $rules = [
            'name'          => 'required',
            'gender'        => 'required',
            'phone'         => 'required|numeric',
            'address'       => 'required',
            'email'         => 'required|email',
            'dob' =>        'required|date|before:tomorrow',
            'nationality'   => 'required',
            'education'     => 'required',
            'contact_way'  => 'required',
        ];

        return $rules;
    }

    public function getRoute()
    {
        return 'fdw.admin.user.add';
    }

    public function getSubmitText()
    {
        return 'Add User';
    }

    public function getRegistrationRoute()
    {
        return 'fdw.user.register';
    }
    public function getButtonName()
    {
        return 'Register';
    }
}
