<?php

namespace Introcept\Core\Http\Repositories;

use Validator;

class ClientRepository
{

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->getAllData($this->getCSVFile());
    }

    /**
     * @return mixed
     */
    public function getCSVFile()
    {
        return storage_path("data/clients.csv");
    }

    /**
     * @param $fileName
     * @param $delimeter
     * @return mixed
     */
    public function getAllData($fileName = "", $delimiter = ",")
    {
        if (!file_exists($fileName) || !is_readable($fileName)) {

            return false;
        }

        $header = null;
        $data = [];
        if (($handle = fopen($fileName, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }

        return $data;
    }

    /**
     * @param $clientInfo
     * @return mixed
     */
    public function storeData($clientInfo)
    {
        $fileName = $this->getCSVFile();

        try {
            if (file_exists($fileName)) {
                $file = fopen($fileName, "a");
                fputcsv($file, $clientInfo);
            } else {
                $file = fopen($fileName, "w");
                $heading = [
                    "Name",
                    "Email",
                    "Phone",
                    "Address",
                    "Date Of Birth",
                    "Nationality",
                    "Educational Background",
                    "Preferred Mode of Contact",
                    "Gender",
                ];
                fputcsv($file, $heading);
                fputcsv($file, $clientInfo);
            }
            fclose($file);

            return true;

        } catch (\Throwable $t) {

            return $this->logger->error("Error Logging", ['Insert' => "Employee Record is not Inserted" . $t]);
        }

    }

    /**
     * @param $clientId
     * @return mixed
     */
    public function getInfo($clientId)
    {
        $fileName = $this->getCSVFile();

        $allClients = $this->getAllData($fileName);

        if (array_key_exists($clientId, $allClients)) {
            return $allClients[$clientId];
        }

        return false;
    }
}
