$(document).ready(function () {
    $('#example').DataTable();
    window.setTimeout(function () {
        $("#success").fadeTo(1000, 0).slideUp(1000, function () {
            $(this).remove();
        });
    }, 4000);

});

$("#newUserEmail").keyup(function () {
    var email = $("#newUserEmail").val();
    $.get(base_url + '/validate-email?email=' + email,
        function (returnedData) {
            if (returnedData == "exists") {

                $('#email-exist').css('display', 'inline-block');
                $(":submit").attr("disabled", true);
            }
            else {
                $(":submit").attr("disabled", false);

                $('#email-exist').css('display', 'none');
            }
        }).fail(function () {
        console.log("error");
    });
});

$("#newUserName").keyup(function () {
    var name = $("#newUserName").val();
    $.get(base_url + '/validate-name?name=' + name,
        function (returnedData) {
            if (returnedData == "exists") {
                $('#user-exist').css('display', 'inline-block');
                $(":submit").attr("disabled", true);
            }
            else {
                $(":submit").attr("disabled", false);

                $('#user-exist').css('display', 'none');
            }
        }).fail(function () {
        console.log("error");
    });
});




  