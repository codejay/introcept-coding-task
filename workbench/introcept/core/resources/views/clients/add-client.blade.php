@extends("layouts.app")

@section("content")
    <div id="main-app">
        <form method="post" action="{{ route('clients.store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
            {{ csrf_field() }}

            <h3>Add Clients</h3>
            <span><i>Please compulsarily fill up the data with *</i></span>
            <hr>
            <div class="row">
                <div class="form-group col-xs-6">
                    <label for="name">Name:</label>
                    <div class="required-item">*</div>
                    <input type="text" name="name" class="form-control" id="newUserName" value="{{ old('name') }} "
                           required>
                    @if($errors->has('name') )
                        <span class="has-error">{{ $errors->first('name') }}</span>
                    @endif
                    <div id="user-exist" style="display : none; color:red" class="help-block">User already exists</div>

                </div>
                <div class="form-group col-xs-6">
                    <label for="email">Email</label>
                    <div class="required-item">*</div>
                    <input type="text" name="email" class="form-control " id="newUserEmail" required>
                    <span class="help-block">{{ $errors->first('email') }}</span>
                    <div id="email-exist" style="display : none; color:red" class="help-block">Email already exists
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-xs-6">
                    <label for="phone">Phone</label>
                    <div class="required-item">*</div>
                    <input type="text" name="phone" class="form-control" id="newUserPhone" required>
                    <span class="help-block">{{ $errors->first('phone')  }} </span>

                </div>
                <div class="form-group col-xs-6">
                    <label for="address">Address</label>
                    <div class="required-item">*</div>
                    <input type="text" name="address" class="form-control" id="newUserPhone">
                    <span class="help-block">{{ $errors->first('address') }}</span>

                </div>
            </div>

            <div class="row">
                <div class="form-group col-xs-6">
                    <label for="dob">Date Of Birth</label>
                    <div class="required-item">*</div>
                    <input type="date" name="dob" class="form-control" id="datepicker" required>
                    <span class="help-block">{{ $errors->first('dob') }}</span>

                </div>
                <div class="form-group col-xs-6">
                    <label for="nationality">Nationaliy</label>
                    <div class="required-item">*</div>
                    <select name="nationality" class="form-control" id="nationality">

                        @foreach($countries as $country)
                            <option {{ $country == "Nepal" ? 'selected="selected"' : '' }} value={{ $country }}>{{ $country }}</option>
                        @endforeach
                    </select>

                    <span class="help-block">{{ $errors->first('nationality') }}</span>

                </div>
            </div>

            <div class="row">
                <div class="form-group col-xs-6">
                    <label for="education">Education</label>
                    <div class="required-item">*</div>
                    <select name="education" class="form-control" id="education">
                        <option value="Literate">Literate</option>
                        <option value="SLC">SLC</option>
                        <option value="Intermediate">Intermediate</option>
                        <option value="Bachelors">Bachelors</option>
                    </select>
                    <span class="help-block">{{ $errors->first('education') }}</span>

                </div>
                <div class="form-group col-xs-6">
                    <label for="contact_way">Preferred Mode Of Contact</label>
                    <div class="required-item">*</div>
                    <select name="contact_way" class="form-control" id="contact_way">
                        <option value="none">None</option>
                        <option value="phone">Phone</option>
                        <option value="email">Email</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-xs-12">
                <label for="gender">Gender:</label>
                <div class="required-item">*</div>
                <p>
                    <input type="radio" name="gender" value="male" checked> Male<br>
                    <input type="radio" name="gender" value="female"> Female<br>
                    <span class="help-block">{{ $errors->first('gender') }}</span>
            </div>
            <input type="hidden" name="_token" value="{{ Session::token() }}" )>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>


@endsection