@extends('layouts.app')
@section('title')
    Client Home Page
@stop
@section('content')
    <div id="main-app">
        <h2>Our Clients</h2>
        <a href="{{ URL::route('clients.create') }}" class="btn btn-default"> Add Clients </a>
        <p>
    </div>
    <hr>

    <div class="">
        @if($clients)
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>DOB</th>
                    <th>Nationality</th>
                    <th>Education</th>
                    <th>Preferred Mode Of Contact</th>
                    <th>Gender</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>
                @foreach($clients as $main_key => $client)
                    <tr>
                        @foreach($client as $key => $data)
                            <td>{{ $client[$key] }}</td>
                        @endforeach
                        <td>
                            <a href="{{ route("clients.show", $main_key) }}">
                                Details
                            </a>
                            {{--  <a href="{{ route("clients.edit", $main_key) }}">
                                 <span class="glyphicon glyphicon-pencil "></span> &nbsp
                             </a>
                             <a href="{{ route("clients.destroy", $main_key) }}">
                                 <span class="glyphicon glyphicon-trash "></span>
                             </a> --}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No Users at the moment..
        @endif
    </div>
@stop
