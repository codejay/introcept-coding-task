@extends('layouts.app')
@section('title')
    View Detail for the specific client.
@stop

@section('content')
    <div class="">
        <h1>{{ $clientInfo['Name'] }}</h1>
        <table class="table table-bordered">
            <tr>
                <td>Gender:</td>
                <td>{{ $clientInfo['Gender'] }}</td>
            </tr>

            <tr>
                <td>Phone:</td>
                <td>{{ $clientInfo['Phone'] }}</td>
            </tr>

            <tr>
                <td>Email:</td>
                <td>{{ $clientInfo['Email'] }}</td>
            </tr>

            <tr>
                <td>Address:</td>
                <td>{{ $clientInfo['Address'] }}</td>
            </tr>

            <tr>
                <td>Nationality:</td>
                <td>{{ $clientInfo['Nationality'] }}</td>
            </tr>

            <tr>
                <td>Date of Birth:</td>
                <td>{{ $clientInfo['Date Of Birth'] }}</td>
            </tr>

            <tr>
                <td>Educational Background:</td>
                <td>{{ $clientInfo['Educational Background'] }}</td>
            </tr>

            <tr>
                <td>Preferred Mode of Contact:</td>
                <td>{{ $clientInfo['Preferred Mode of Contact'] }}</td>
            </tr>
        </table>
    </div>
@stop